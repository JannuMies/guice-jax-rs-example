package net.talviuni.services;

import net.talviuni.interfaces.FooInterface;
import net.talviuni.models.Named;
import net.talviuni.models.Reacher;

public class FooService implements FooInterface {

    @Override
    public String foo() {
        return "This is a FooService\n";
    }

    @Override
    public Reacher getReacher(final String name) {
        final Reacher result = new Reacher();
        result.setName(name);
        return result;
    }

    @Override
    public Reacher fromName(final Named named) {
        return getReacher(named.getName());
    }
}
