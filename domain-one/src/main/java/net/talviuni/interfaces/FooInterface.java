package net.talviuni.interfaces;

import net.talviuni.models.Named;
import net.talviuni.models.Reacher;

public interface FooInterface {
    public String foo();

    public Reacher getReacher(String name);

    public Reacher fromName(Named named);
}
