package net.talviuni.models;

@jakarta.xml.bind.annotation.XmlRootElement
public class Reacher {
    public final boolean UNEXPECTED = true;
    private String name;

    public Reacher() {
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }
}
