package net.talviuni.models;

@jakarta.xml.bind.annotation.XmlRootElement
public class Named {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }
}
