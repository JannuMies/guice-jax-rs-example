package net.talviuni.endpoints;

import jakarta.inject.Inject;
import jakarta.ws.rs.Path;
import net.talviuni.endpoints.thing.ThingResource;

@Path("/one")
public class DomainOneRootResource {
    final ThingResource thingResource;

    @Inject
    public DomainOneRootResource(final ThingResource thingResource) {
        this.thingResource = thingResource;
    }

    @Path("thing")
    public ThingResource thingResource() {
        return thingResource;
    }
}
