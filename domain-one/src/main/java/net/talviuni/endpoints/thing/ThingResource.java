package net.talviuni.endpoints.thing;

import jakarta.inject.Inject;

import io.swagger.v3.oas.annotations.Operation;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import net.talviuni.interfaces.FooInterface;
import net.talviuni.models.Named;
import net.talviuni.models.Reacher;

public class ThingResource {

    private final FooInterface fooImpl;

    @Inject
    public ThingResource(final FooInterface fooImpl) {
        this.fooImpl = fooImpl;
    }

    @GET
    @Operation(description = "Get a thing")
    public String foo() {
        return fooImpl.foo();
    }

    @GET
    @Path("{name}")
    @Operation(description = "Get a reacher named by the input string. They're usually going for the unexpected.", operationId = "reachTheUnexpected")
    @Produces(MediaType.APPLICATION_JSON)
    public Reacher bar(final @PathParam("name") String name) {
        return fooImpl.getReacher(name);
    }

    @POST
    @Path("fromNamed")
    @Operation(description = "Get a reacher with a known Named-model. They're usually going for the unexpected.", operationId = "reachTheNamedUnexpected")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Reacher fromNamed(final Named named) {
        return fooImpl.fromName(named);
    }
}
