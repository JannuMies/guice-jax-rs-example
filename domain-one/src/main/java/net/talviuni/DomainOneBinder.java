package net.talviuni;

import jakarta.inject.Singleton;
import net.talviuni.endpoints.thing.ThingResource;
import net.talviuni.interfaces.FooInterface;
import net.talviuni.services.FooService;
import org.glassfish.hk2.utilities.binding.AbstractBinder;

public class DomainOneBinder extends AbstractBinder {
    @Override
    protected void configure() {
        bind(ThingResource.class).to(ThingResource.class).in(Singleton.class);
        bind(FooService.class).to(FooInterface.class).in(Singleton.class);
    }
}
