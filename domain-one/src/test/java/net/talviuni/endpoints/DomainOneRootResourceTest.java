package net.talviuni.endpoints;

import org.testng.Assert;
import org.testng.annotations.Test;

import net.talviuni.endpoints.thing.ThingResource;

public class DomainOneRootResourceTest {

    @Test
    public void testNullThingResourceReturnsNull() {
        DomainOneRootResource res = new DomainOneRootResource(null);

        Assert.assertNull(res.thingResource());
    }

    @Test
    public void testNotNullThingResourceReturnsSomething() {
        ThingResource thingResource = new ThingResource(null);
        DomainOneRootResource res = new DomainOneRootResource(thingResource);

        Assert.assertNotNull(res.thingResource());
    }
}
