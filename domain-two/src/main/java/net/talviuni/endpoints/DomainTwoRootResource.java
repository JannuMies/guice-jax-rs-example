package net.talviuni.endpoints;

import io.swagger.v3.oas.annotations.Operation;
import jakarta.inject.Inject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;

@Path("/two")
public class DomainTwoRootResource {

    @Inject
    public DomainTwoRootResource() {
    }

    @GET
    @Operation(description = "get yoinks")
    public String foo() {
        return "yoinks\n";
    }
}
