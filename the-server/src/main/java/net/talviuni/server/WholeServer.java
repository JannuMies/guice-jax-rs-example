package net.talviuni.server;

import java.net.URI;
import net.talviuni.endpoints.WebAppBase;
import org.eclipse.jetty.server.Server;
import org.glassfish.jersey.jetty.JettyHttpContainerFactory;
import org.glassfish.jersey.server.ResourceConfig;

public class WholeServer {
    private static final Integer PORT = 8080;
    private static final String HOST = "localhost";
    private static final URI serverUri = URI.create("http://" + HOST + ":" + PORT.toString());

    public static void main(String[] args) throws Exception {
        final Server server;
        server = getServer();
        server.start();
    }

    private static Server getServer() {
        return JettyHttpContainerFactory.createServer(serverUri, getWebApp(), false);
    }

    private static ResourceConfig getWebApp() {
        final ResourceConfig result = new WebAppBase();
        return result;
    }
}
