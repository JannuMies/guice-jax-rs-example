package net.talviuni.endpoints;

import io.swagger.v3.jaxrs2.integration.resources.OpenApiResource;
import net.talviuni.DomainOneBinder;

import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.server.ResourceConfig;

public class WebAppBase extends ResourceConfig {
    public WebAppBase() {
        packages("net.talviuni.endpoints"); // Scan the package for endpoints
        register(new OpenApiResource()); // Swagger generation
        register(JacksonFeature.class); // JSON-mapping
        register(new DomainOneBinder()); // Dependency Injection
    }
}
